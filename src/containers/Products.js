import React, {useContext} from 'react';
import ProductItem from '../components/Products/ProductItem';
import './Products.css';
import {AppContext} from '../context/AppContext';

const Products = props => {
  const appContext = useContext(AppContext);
  const {mainProducts} = appContext
  return (
    <ul className="products-list">
      {mainProducts.map(prod => (
        <ProductItem
          key={`product-${prod.id}`}
          id={prod.id}
          title={prod.title}
          description={prod.description}
          isFav={prod.isFavorite}
        />
      ))}
    </ul>
  );
};

export default Products;
