import React, {useContext} from 'react';
import Card from '../UI/Card';
import './ProductItem.css';
import {AppContext} from '../../context/AppContext';

const ProductItem = props => {

  const appContext = useContext(AppContext)
  const {setAsFavourite} = appContext

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <div className="product-item">
        <h2 className={props.isFav ? 'is-fav' : ''}>{props.title}</h2>
        <p>{props.description}</p>
        <button
          className={!props.isFav ? 'button-outline' : ''}
          onClick={() => setAsFavourite(props.id)}
        >
          {props.isFav ? 'Un-Favorite' : 'Favorite'}
        </button>
      </div>
    </Card>
  );
};

export default ProductItem;
