import React, {useState, useEffect} from 'react';
import {products} from './initialProducts';

export const AppContext = React.createContext({
    mainProducts: [],
    favouriteProducts: [],
    setAsFavourite: () => {},
});

const AppContextProvider = (props) => {
    const [mainProducts, setMainProducts] = useState(products);
    const [favouriteProducts, setFavouriteProducts] = useState([]);

    const setAsFavourite = (productId) => {
        const modifiedProducts = mainProducts.reduce((acc, item) => {
            if (item.id === productId) {
                return [
                    ...acc, {
                        ...item,
                        isFavorite: !item.isFavorite,
                    }];
            }
            return [...acc, item];
        }, []);
        setMainProducts(modifiedProducts);
    };

    useEffect(() => {
        const newFavouriteProducts = mainProducts.filter(p => p.isFavorite);
        setFavouriteProducts(newFavouriteProducts);
    }, [mainProducts]);

    const context = {
        mainProducts,
        favouriteProducts,
        setAsFavourite,
    };

    return (
        <AppContext.Provider value={context}>
            {props.children}
        </AppContext.Provider>
    );
};

export default AppContextProvider;
